# line-graph

## Requirements

The data in the data.json file represent the data point to be plotted on a line graph.

- Write a function that having a start_date and an end_date as input returns the subset of data included between the two for the slug `aggregation-overall` and for the key `score`

  - Assume the start_date and end_date exactly match the `x` key in the serie
  - start_date and end_date must be included in the returned data.
  - The series always contains start_date and end_date

  Input:
  start_date: "2015-08-19T14:00:19.352000Z"
  end_date: "2015-10-12T07:27:47.493000Z"

  Expected result:

  ```json
  [
    {
      "y": 282,
      "x": "2015-08-19T14:00:19.352000Z"
    },
    {
      "y": 227,
      "x": "2015-10-08T14:45:31.991000Z"
    },
    {
      "y": 185,
      "x": "2015-10-12T07:27:47.493000Z"
    }
  ]
  ```

- Write the same function as above to match the case that:

  - The series does not always contains end_date or start_date
  - Start_date and end_date don't match the `x` key in the serie

- Consider that we want to display the data with the key `extra` on mouse over on a point of the key `score`. Write a function to format the data for this use case.

## Project Structure

I have built 2 factories that are able to:

1. extract scores within a time range
1. extract scores and their associated `extra` within a time range

The inclusiveness of `start_date` and `end_date` are achieved through function compositions (see `datesComparison.ts`).

### Scripts

#### `npm run build`

It compiles typescript files with `tsc` and emit a `build` folder.

#### `npm test`

To ensure requirements are always met using `ts-jest`.

#### `npm start`

To run examples of calling the various versions of `extractScores` exploiting node's `perf_hooks` to observe functions' execution duration.

It will emit a log of how many scores have been extracted and the milliseconds each function required to run.

(take a look at `performanceObserver.ts` )

#### `npm run benchmark`

`cronometro` benchmarks how the various version of `extractScores` behave when running on increasing size datasets.

The aim is to always grow linear O(n): if the results of the benchmark fail to suite this condition, this step will fail too.

### CI

I've setup GitLab CI to run `test`, `start` and in particular `benchmark` at every commit to spot any performance regression.
