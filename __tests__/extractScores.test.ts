import { extractScoresFactory } from "../src/extractScores";
import {
  isDateContainedInclusive,
  isDateContained,
} from "../src/datesComparison";
import rawData from "../src/data/data.json";

test("returns scores filtered by dates (inclusive)", () => {
  const extractScoresInclusive = extractScoresFactory(isDateContainedInclusive);
  const expected = {
    start_date: "2015-08-19T14:00:19.352000Z",
    end_date: "2015-10-12T07:27:47.493000Z",
    scores: [
      {
        y: 282,
        x: "2015-08-19T14:00:19.352000Z",
      },
      {
        y: 227,
        x: "2015-10-08T14:45:31.991000Z",
      },
      {
        y: 185,
        x: "2015-10-12T07:27:47.493000Z",
      },
    ],
  };

  const actual = extractScoresInclusive(
    "2015-08-19T14:00:19.352000Z",
    "2015-10-12T07:27:47.493000Z",
    rawData
  );

  expect(actual).toEqual(expected);
});

test("returns scores filtered by dates (NOT inclusive)", () => {
  const extractScores = extractScoresFactory(isDateContained);

  const expected = {
    start_date: "2015-08-19T14:00:19.352000Z",
    end_date: "2015-10-12T07:27:47.493000Z",
    scores: [
      {
        y: 227,
        x: "2015-10-08T14:45:31.991000Z",
      },
    ],
  };

  const actual = extractScores(
    "2015-08-19T14:00:19.352000Z",
    "2015-10-12T07:27:47.493000Z",
    rawData
  );

  expect(actual).toEqual(expected);
});
