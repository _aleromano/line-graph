import { extractScoresWithExtrasFactory } from "../src/extractScoresWithExtras";
import {
  isDateContained,
  isDateContainedInclusive,
} from "../src/datesComparison";
import rawData from "../src/data/data.json";

test("returns scores filtered by dates inclusive and mouse over extra data", () => {
  const extractScoresInclusiveWithExtras = extractScoresWithExtrasFactory(
    isDateContainedInclusive
  );
  const expected = {
    start_date: "2015-08-19T14:00:19.352000Z",
    end_date: "2015-10-12T07:27:47.493000Z",
    scores: [
      {
        y: 282,
        x: "2015-08-19T14:00:19.352000Z",
        extra: {
          quiz_session_type: "Study",
          priority: 282,
          score_delta: null,
          quiz_session: 6775,
          quiz_config: 226,
          quiz_config_title: "Platform Reference for AWS",
        },
      },
      {
        y: 227,
        x: "2015-10-08T14:45:31.991000Z",
        extra: {
          quiz_session_type: "Study",
          priority: 55,
          score_delta: -55,
          quiz_session: 19037,
          quiz_config: 226,
          quiz_config_title: "Platform Reference for AWS",
        },
      },
      {
        y: 185,
        x: "2015-10-12T07:27:47.493000Z",
        extra: {
          quiz_session_type: "Study",
          priority: 42,
          score_delta: -42,
          quiz_session: 19337,
          quiz_config: 226,
          quiz_config_title: "Platform Reference for AWS",
        },
      },
    ],
  };

  const actual = extractScoresInclusiveWithExtras(
    "2015-08-19T14:00:19.352000Z",
    "2015-10-12T07:27:47.493000Z",
    rawData
  );

  expect(actual).toEqual(expected);
});

test("returns scores filtered by dates (NOT inclusive) and mouse over extra data", () => {
  const extractScoresWithExtras = extractScoresWithExtrasFactory(
    isDateContained
  );
  const expected = {
    start_date: "2015-08-19T14:00:19.352000Z",
    end_date: "2015-10-12T07:27:47.493000Z",
    scores: [
      {
        y: 227,
        x: "2015-10-08T14:45:31.991000Z",
        extra: {
          quiz_session_type: "Study",
          priority: 55,
          score_delta: -55,
          quiz_session: 19037,
          quiz_config: 226,
          quiz_config_title: "Platform Reference for AWS",
        },
      },
    ],
  };

  const actual = extractScoresWithExtras(
    "2015-08-19T14:00:19.352000Z",
    "2015-10-12T07:27:47.493000Z",
    rawData
  );

  expect(actual).toEqual(expected);
});
