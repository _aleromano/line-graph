import { cronometro, Results, Tests } from "cronometro";
import { EXTRACT_SCORES_HEAD, EXTRACT_SCORES_TAIL } from "./extractScores";
import {
  EXTRACT_SCORES_INCLUSIVE_HEAD,
  EXTRACT_SCORES_INCLUSIVE_TAIL,
} from "./extractScoresInclusive";
import {
  EXTRACT_SCORES_INCLUSIVE_WITH_EXTRA_HEAD,
  EXTRACT_SCORES_INCLUSIVE_WITH_EXTRA_TAIL,
} from "./extractScoresInclusiveWithExtra";
import {
  EXTRACT_SCORES_WITH_EXTRA_HEAD,
  EXTRACT_SCORES_WITH_EXTRA_TAIL,
} from "./extractScoresWithExtra";

benchmark(EXTRACT_SCORES_HEAD);
benchmark(EXTRACT_SCORES_TAIL);
benchmark(EXTRACT_SCORES_INCLUSIVE_HEAD);
benchmark(EXTRACT_SCORES_INCLUSIVE_TAIL);
benchmark(EXTRACT_SCORES_WITH_EXTRA_HEAD);
benchmark(EXTRACT_SCORES_WITH_EXTRA_TAIL);
benchmark(EXTRACT_SCORES_INCLUSIVE_WITH_EXTRA_HEAD);
benchmark(EXTRACT_SCORES_INCLUSIVE_WITH_EXTRA_TAIL);

function benchmark({ tests, testName }: { tests: Tests; testName: string }) {
  return cronometro(
    tests,
    {
      iterations: 500,
      print: {
        compare: true,
        compareMode: "previous",
      },
    },
    (err, results) => handleResults(err, results, testName)
  );
}

function handleResults(err: Error | null, results: Results, testName: string) {
  if (err) {
    throw err;
  }

  if (areGrowingLinear(results)) {
    console.log(`Well done, ${testName} is growing O(n)`);
  } else {
    throw new Error(`!!! ${testName} is growing worse than O(n)!!!`);
  }
}

// Given we are doubling the dataset, if exceution time grows double (with 0.3 tolerance) too we are fine
function areGrowingLinear(results: Results) {
  return Object.values(results).every((result, i) => {
    if (Object.values(results)[i + 1] !== undefined) {
      return Object.values(results)[i + 1].mean / result.mean < 2.3;
    }

    return true;
  });
}
