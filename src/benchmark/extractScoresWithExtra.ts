import {
  doubleRawData,
  END_DATE_HEAD_OF_SERIE,
  END_DATE_TAIL_OF_SERIE,
  octupleRawData,
  quadrupleRawData,
  rawData,
  START_DATE_HEAD_OF_SERIE,
  START_DATE_TAIL_OF_SERIE,
} from "../data";
import { isDateContained } from "../datesComparison";
import { extractScoresWithExtrasFactory } from "../extractScoresWithExtras";

const extractScoresWithExtra = extractScoresWithExtrasFactory(isDateContained);

export const EXTRACT_SCORES_WITH_EXTRA_HEAD = {
  tests: {
    single: () =>
      extractScoresWithExtra(START_DATE_HEAD_OF_SERIE, END_DATE_HEAD_OF_SERIE, rawData),
    double: () =>
      extractScoresWithExtra(
        START_DATE_HEAD_OF_SERIE,
        END_DATE_HEAD_OF_SERIE,
        doubleRawData
      ),
    quadruple: () =>
      extractScoresWithExtra(
        START_DATE_HEAD_OF_SERIE,
        END_DATE_HEAD_OF_SERIE,
        quadrupleRawData
      ),
    octuple: () =>
      extractScoresWithExtra(
        START_DATE_HEAD_OF_SERIE,
        END_DATE_HEAD_OF_SERIE,
        octupleRawData
      ),
  },
  testName: "extractScoresWithExtra_head",
};
export const EXTRACT_SCORES_WITH_EXTRA_TAIL = {
  tests: {
    single_tail: () =>
      extractScoresWithExtra(START_DATE_TAIL_OF_SERIE, END_DATE_TAIL_OF_SERIE, rawData),
    double_tail: () =>
      extractScoresWithExtra(
        START_DATE_TAIL_OF_SERIE,
        END_DATE_TAIL_OF_SERIE,
        doubleRawData
      ),
    quadruple_tail: () =>
      extractScoresWithExtra(
        START_DATE_TAIL_OF_SERIE,
        END_DATE_TAIL_OF_SERIE,
        quadrupleRawData
      ),
    octuple_tail: () =>
      extractScoresWithExtra(
        START_DATE_TAIL_OF_SERIE,
        END_DATE_TAIL_OF_SERIE,
        octupleRawData
      ),
  },
  testName: "extractScoresWithExtra_tail",
};
