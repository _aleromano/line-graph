import {
  doubleRawData,
  END_DATE_HEAD_OF_SERIE,
  END_DATE_TAIL_OF_SERIE,
  octupleRawData,
  quadrupleRawData,
  rawData,
  START_DATE_HEAD_OF_SERIE,
  START_DATE_TAIL_OF_SERIE,
} from "../data";
import { isDateContainedInclusive } from "../datesComparison";
import { extractScoresFactory } from "../extractScores";

const extractScoresInclusive = extractScoresFactory(isDateContainedInclusive);

export const EXTRACT_SCORES_INCLUSIVE_HEAD = {
  tests: {
    single: () =>
      extractScoresInclusive(
        START_DATE_HEAD_OF_SERIE,
        END_DATE_HEAD_OF_SERIE,
        rawData
      ),
    double: () =>
      extractScoresInclusive(
        START_DATE_HEAD_OF_SERIE,
        END_DATE_HEAD_OF_SERIE,
        doubleRawData
      ),
    quadruple: () =>
      extractScoresInclusive(
        START_DATE_HEAD_OF_SERIE,
        END_DATE_HEAD_OF_SERIE,
        quadrupleRawData
      ),
    octuple: () =>
      extractScoresInclusive(
        START_DATE_HEAD_OF_SERIE,
        END_DATE_HEAD_OF_SERIE,
        octupleRawData
      ),
  },
  testName: "extractScoresInclusive_head",
};
export const EXTRACT_SCORES_INCLUSIVE_TAIL = {
  tests: {
    single: () =>
      extractScoresInclusive(
        START_DATE_TAIL_OF_SERIE,
        END_DATE_TAIL_OF_SERIE,
        rawData
      ),
    double_tail: () =>
      extractScoresInclusive(
        START_DATE_TAIL_OF_SERIE,
        END_DATE_TAIL_OF_SERIE,
        doubleRawData
      ),
    quadruple_tail: () =>
      extractScoresInclusive(
        START_DATE_TAIL_OF_SERIE,
        END_DATE_TAIL_OF_SERIE,
        quadrupleRawData
      ),
    octuple_tail: () =>
      extractScoresInclusive(
        START_DATE_TAIL_OF_SERIE,
        END_DATE_TAIL_OF_SERIE,
        octupleRawData
      ),
  },
  testName: "extractScoresInclusive_tail",
};
