import { extractScoresFactory } from "../extractScores";
import { isDateContained } from "../datesComparison";
import {
  rawData,
  doubleRawData,
  quadrupleRawData,
  octupleRawData,
  START_DATE_HEAD_OF_SERIE,
  END_DATE_HEAD_OF_SERIE,
  START_DATE_TAIL_OF_SERIE,
  END_DATE_TAIL_OF_SERIE,
} from "../data";

const extractScores = extractScoresFactory(isDateContained);

export const EXTRACT_SCORES_HEAD = {
  tests: {
    single: () =>
      extractScores(START_DATE_HEAD_OF_SERIE, END_DATE_HEAD_OF_SERIE, rawData),
    double: () =>
      extractScores(
        START_DATE_HEAD_OF_SERIE,
        END_DATE_HEAD_OF_SERIE,
        doubleRawData
      ),
    quadruple: () =>
      extractScores(
        START_DATE_HEAD_OF_SERIE,
        END_DATE_HEAD_OF_SERIE,
        quadrupleRawData
      ),
    octuple: () =>
      extractScores(
        START_DATE_HEAD_OF_SERIE,
        END_DATE_HEAD_OF_SERIE,
        octupleRawData
      ),
  },
  testName: "extractScores_head",
};
export const EXTRACT_SCORES_TAIL = {
  tests: {
    single_tail: () =>
      extractScores(START_DATE_TAIL_OF_SERIE, END_DATE_TAIL_OF_SERIE, rawData),
    double_tail: () =>
      extractScores(
        START_DATE_TAIL_OF_SERIE,
        END_DATE_TAIL_OF_SERIE,
        doubleRawData
      ),
    quadruple_tail: () =>
      extractScores(
        START_DATE_TAIL_OF_SERIE,
        END_DATE_TAIL_OF_SERIE,
        quadrupleRawData
      ),
    octuple_tail: () =>
      extractScores(
        START_DATE_TAIL_OF_SERIE,
        END_DATE_TAIL_OF_SERIE,
        octupleRawData
      ),
  },
  testName: "extractScores_tail",
};
