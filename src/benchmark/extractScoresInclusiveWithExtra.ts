import {
  doubleRawData,
  END_DATE_HEAD_OF_SERIE,
  END_DATE_TAIL_OF_SERIE,
  octupleRawData,
  quadrupleRawData,
  rawData,
  START_DATE_HEAD_OF_SERIE,
  START_DATE_TAIL_OF_SERIE,
} from "../data";
import { isDateContainedInclusive } from "../datesComparison";
import { extractScoresWithExtrasFactory } from "../extractScoresWithExtras";

const extractScoresInclusiveWithExtra = extractScoresWithExtrasFactory(
  isDateContainedInclusive
);

export const EXTRACT_SCORES_INCLUSIVE_WITH_EXTRA_HEAD = {
  tests: {
    single: () =>
      extractScoresInclusiveWithExtra(
        START_DATE_HEAD_OF_SERIE,
        END_DATE_HEAD_OF_SERIE,
        rawData
      ),
    double: () =>
      extractScoresInclusiveWithExtra(
        START_DATE_HEAD_OF_SERIE,
        END_DATE_HEAD_OF_SERIE,
        doubleRawData
      ),
    quadruple: () =>
      extractScoresInclusiveWithExtra(
        START_DATE_HEAD_OF_SERIE,
        END_DATE_HEAD_OF_SERIE,
        quadrupleRawData
      ),
    octuple: () =>
      extractScoresInclusiveWithExtra(
        START_DATE_HEAD_OF_SERIE,
        END_DATE_HEAD_OF_SERIE,
        octupleRawData
      ),
  },
  testName: "extractScoresInclusiveWithExtra_head",
};
export const EXTRACT_SCORES_INCLUSIVE_WITH_EXTRA_TAIL = {
  tests: {
    single_tail: () =>
      extractScoresInclusiveWithExtra(
        START_DATE_TAIL_OF_SERIE,
        END_DATE_TAIL_OF_SERIE,
        rawData
      ),
    double_tail: () =>
      extractScoresInclusiveWithExtra(
        START_DATE_TAIL_OF_SERIE,
        END_DATE_TAIL_OF_SERIE,
        doubleRawData
      ),
    quadruple_tail: () =>
      extractScoresInclusiveWithExtra(
        START_DATE_TAIL_OF_SERIE,
        END_DATE_TAIL_OF_SERIE,
        quadrupleRawData
      ),
    octuple_tail: () =>
      extractScoresInclusiveWithExtra(
        START_DATE_TAIL_OF_SERIE,
        END_DATE_TAIL_OF_SERIE,
        octupleRawData
      ),
  },
  testName: "extractScoresInclusiveWithExtra_tail",
};
