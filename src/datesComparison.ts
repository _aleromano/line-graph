export type DatesComparison = (
  date: string,
  startDate: string,
  endDate: string
) => boolean;

export const isDateContainedInclusive: DatesComparison = (
  scoreDate,
  startDate,
  endDate
) => {
  const serieDate = new Date(scoreDate);
  const startDateJs = new Date(startDate);
  const endDateJs = new Date(endDate);
  return serieDate <= endDateJs && serieDate >= startDateJs;
};

export const isDateContained: DatesComparison = (
  scoreDate,
  startDate,
  endDate
) => {
  const serieDate = new Date(scoreDate);
  const startDateJs = new Date(startDate);
  const endDateJs = new Date(endDate);
  return serieDate < endDateJs && serieDate > startDateJs;
};
