import { areScores, ExtractScoresFactory, Result, Score } from "./domain";

export const extractScoresFactory: ExtractScoresFactory<Result<Score>> = (
  dateComparison
) => (startDate, endDate, rawData) => {
  const scores = rawData.data
    .find((series: any) => series.slug === "aggregation-overall")!
    .details.find((detail: any) => detail.key === "score")!.series;

  const filteredScores = areScores(scores)
    ? scores.filter((score) => {
        return dateComparison(score.x, startDate, endDate);
      })
    : [];

  return {
    start_date: startDate,
    end_date: endDate,
    scores: filteredScores,
  };
};
