import {
  areExtras,
  areScores,
  ExtractScoresFactory,
  Result,
  ScoreWithExtras,
} from "./domain";

export const extractScoresWithExtrasFactory: ExtractScoresFactory<Result<
  ScoreWithExtras
>> = (dateComparison) => (startDate, endDate, rawData) => {
  const scores = rawData.data
    .find((series: any) => series.slug === "aggregation-overall")!
    .details.find((detail: any) => detail.key === "score")!.series;

  const filteredScores = areScores(scores)
    ? scores.filter((score) => {
        return dateComparison(score.x, startDate, endDate);
      })
    : [];

  const filteredScoresWithExtras = filteredScores.map((score) => {
    const extras = rawData.data
      .find((series: any) => series.slug === "aggregation-overall")!
      .details.find((detail: any) => detail.key === "extra")!.series;

    const associatedExtra = areExtras(extras)
      ? extras.find((e) => e.x === score.x)!.y
      : null;

    return {
      ...score,
      extra: associatedExtra,
    };
  });

  return {
    start_date: startDate,
    end_date: endDate,
    scores: filteredScoresWithExtras,
  };
};
