import deepcopy from "deepcopy";
import rawData from "./data.json";

const START_DATE_HEAD_OF_SERIE = "2015-08-19T14:00:19.352000Z";
const END_DATE_HEAD_OF_SERIE = "2015-10-12T07:27:47.493000Z";

const START_DATE_TAIL_OF_SERIE = "2019-11-05T13:57:02.921731Z";
const END_DATE_TAIL_OF_SERIE = "2019-11-19T17:14:34.796982Z";

const scores = rawData.data[0].details[0].series;
const extras = rawData.data[0].details[1].series;

const doubleRawData = deepcopy(rawData);
// @ts-ignore
doubleRawData.data[0].details[0].series = [...scores, ...scores];
// @ts-ignore
doubleRawData.data[0].details[1].series = [...extras, ...extras];

const quadrupleRawData = deepcopy(rawData);
// @ts-ignore
quadrupleRawData.data[0].details[0].series = [
  ...scores,
  ...scores,
  ...scores,
  ...scores,
];
// @ts-ignore
quadrupleRawData.data[0].details[1].series = [
  ...extras,
  ...extras,
  ...extras,
  ...extras,
];

const octupleRawData = deepcopy(rawData);
// @ts-ignore
octupleRawData.data[0].details[0].series = [
  ...scores,
  ...scores,
  ...scores,
  ...scores,
  ...scores,
  ...scores,
  ...scores,
  ...scores,
];
// @ts-ignore
octupleRawData.data[0].details[1].series = [
  ...extras,
  ...extras,
  ...extras,
  ...extras,
  ...extras,
  ...extras,
  ...extras,
  ...extras,
];

export {
  rawData,
  doubleRawData,
  quadrupleRawData,
  octupleRawData,
  START_DATE_HEAD_OF_SERIE,
  START_DATE_TAIL_OF_SERIE,
  END_DATE_HEAD_OF_SERIE,
  END_DATE_TAIL_OF_SERIE,
};
