import { DatesComparison } from "./datesComparison";

export type ExtraBody = {
  quiz_session_type: string;
  priority: number;
  score_delta: number;
  quiz_session: number;
  quiz_config: number;
  quiz_config_title: string;
};
export type Extra = {
  y: ExtraBody;
  x: string;
};

export type Score = { y: number; x: string };
export type ScoreWithExtras = Score & {
  extra: ExtraBody | null;
};

export type Result<T> = {
  start_date: string;
  end_date: string;
  scores: T[];
};

export type ExtractScores<T> = (
  startDate: string,
  endDate: string,
  rawData: any
) => T;

export type ExtractScoresFactory<T> = (
  datesComparison: DatesComparison,
) => ExtractScores<T>;


export function areScores(scores: object[]): scores is Score[] {
    return (scores[0] as Score).x !== undefined;
  }
  
  export function areExtras(extras: object[]): extras is Extra[] {
    return (extras[0] as Extra).x !== undefined;
  }
