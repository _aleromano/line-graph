import { PerformanceObserver, performance } from "perf_hooks";

const obs = new PerformanceObserver((list) => {
  const entry = list.getEntries()[0];
  console.log(`Duration: `, entry.duration, 'ms');
});

export const startObserving = () => {
  obs.observe({ entryTypes: ["function"], buffered: false });
  return { observer: obs, performance };
};
