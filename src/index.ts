import {
  doubleRawData,
  END_DATE_HEAD_OF_SERIE,
  END_DATE_TAIL_OF_SERIE,
  octupleRawData,
  quadrupleRawData,
  rawData,
  START_DATE_HEAD_OF_SERIE,
  START_DATE_TAIL_OF_SERIE,
} from "./data";
import { isDateContained, isDateContainedInclusive } from "./datesComparison";
import { extractScoresFactory } from "./extractScores";
import { extractScoresWithExtrasFactory } from "./extractScoresWithExtras";
import { startObserving } from "./performanceObserver";

const { observer, performance } = startObserving();

const extractScoresInclusive = performance.timerify(
  extractScoresFactory(isDateContainedInclusive)
);

const extractScores = performance.timerify(
  extractScoresFactory(isDateContained)
);

const extractScoresWithExtras = performance.timerify(
  extractScoresWithExtrasFactory(isDateContained)
);

const extractScoresInclusiveWithExtras = performance.timerify(
  extractScoresWithExtrasFactory(isDateContainedInclusive)
);

console.log(
  "Single dataset NOT inclusive (HEAD SERIE): ",
  extractScores(START_DATE_HEAD_OF_SERIE, END_DATE_HEAD_OF_SERIE, rawData)
    .scores.length,
  "\n"
);
console.log(
  "Double dataset NOT inclusive (HEAD SERIE): ",
  extractScores(START_DATE_HEAD_OF_SERIE, END_DATE_HEAD_OF_SERIE, doubleRawData)
    .scores.length,
  "\n"
);
console.log(
  "Quadruple dataset NOT inclusive (HEAD SERIE): ",
  extractScores(
    START_DATE_HEAD_OF_SERIE,
    END_DATE_HEAD_OF_SERIE,
    quadrupleRawData
  ).scores.length,
  "\n"
);
console.log(
  "Octuple dataset NOT inclusive (HEAD SERIE): ",
  extractScores(
    START_DATE_HEAD_OF_SERIE,
    END_DATE_HEAD_OF_SERIE,
    octupleRawData
  ).scores.length,
  "\n"
);

console.log(
  "Single dataset NOT inclusive (TAIL SERIE): ",
  extractScores(START_DATE_TAIL_OF_SERIE, END_DATE_TAIL_OF_SERIE, rawData)
    .scores.length,
  "\n"
);
console.log(
  "Double dataset NOT inclusive (TAIL SERIE): ",
  extractScores(START_DATE_TAIL_OF_SERIE, END_DATE_TAIL_OF_SERIE, doubleRawData)
    .scores.length,
  "\n"
);
console.log(
  "Quadruple dataset NOT inclusive (TAIL SERIE): ",
  extractScores(
    START_DATE_TAIL_OF_SERIE,
    END_DATE_TAIL_OF_SERIE,
    quadrupleRawData
  ).scores.length,
  "\n"
);
console.log(
  "Octuple dataset NOT inclusive (TAIL SERIE): ",
  extractScores(
    START_DATE_TAIL_OF_SERIE,
    END_DATE_TAIL_OF_SERIE,
    octupleRawData
  ).scores.length,
  "\n"
);

console.log(
  "Single dataset inclusive (HEAD SERIE): ",
  extractScoresInclusive(
    START_DATE_HEAD_OF_SERIE,
    END_DATE_HEAD_OF_SERIE,
    rawData
  ).scores.length,
  "\n"
);
console.log(
  "Double dataset inclusive (HEAD SERIE): ",
  extractScoresInclusive(
    START_DATE_HEAD_OF_SERIE,
    END_DATE_HEAD_OF_SERIE,
    doubleRawData
  ).scores.length,
  "\n"
);
console.log(
  "Quadruple dataset inclusive (HEAD SERIE): ",
  extractScoresInclusive(
    START_DATE_HEAD_OF_SERIE,
    END_DATE_HEAD_OF_SERIE,
    quadrupleRawData
  ).scores.length,
  "\n"
);
console.log(
  "Octuple dataset inclusive (HEAD SERIE): ",
  extractScoresInclusive(
    START_DATE_HEAD_OF_SERIE,
    END_DATE_HEAD_OF_SERIE,
    octupleRawData
  ).scores.length,
  "\n"
);

console.log(
  "Single dataset inclusive (TAIL SERIE): ",
  extractScoresInclusive(
    START_DATE_TAIL_OF_SERIE,
    END_DATE_TAIL_OF_SERIE,
    rawData
  ).scores.length,
  "\n"
);
console.log(
  "Double dataset inclusive (TAIL SERIE): ",
  extractScoresInclusive(
    START_DATE_TAIL_OF_SERIE,
    END_DATE_TAIL_OF_SERIE,
    doubleRawData
  ).scores.length,
  "\n"
);
console.log(
  "Quadruple dataset inclusive (TAIL SERIE): ",
  extractScoresInclusive(
    START_DATE_TAIL_OF_SERIE,
    END_DATE_TAIL_OF_SERIE,
    quadrupleRawData
  ).scores.length,
  "\n"
);
console.log(
  "Octuple dataset inclusive (TAIL SERIE): ",
  extractScoresInclusive(
    START_DATE_TAIL_OF_SERIE,
    END_DATE_TAIL_OF_SERIE,
    octupleRawData
  ).scores.length,
  "\n"
);

console.log(
  "Single dataset NOT inclusive with extras (HEAD SERIE): ",
  extractScoresWithExtras(
    START_DATE_HEAD_OF_SERIE,
    END_DATE_HEAD_OF_SERIE,
    rawData
  ).scores.length,
  "\n"
);
console.log(
  "Double dataset NOT inclusive with extras (HEAD SERIE): ",
  extractScoresWithExtras(
    START_DATE_HEAD_OF_SERIE,
    END_DATE_HEAD_OF_SERIE,
    doubleRawData
  ).scores.length,
  "\n"
);
console.log(
  "Quadruple dataset NOT inclusive with extras (HEAD SERIE): ",
  extractScoresWithExtras(
    START_DATE_HEAD_OF_SERIE,
    END_DATE_HEAD_OF_SERIE,
    quadrupleRawData
  ).scores.length,
  "\n"
);
console.log(
  "Octuple dataset NOT inclusive with extras (HEAD SERIE): ",
  extractScoresWithExtras(
    START_DATE_HEAD_OF_SERIE,
    END_DATE_HEAD_OF_SERIE,
    octupleRawData
  ).scores.length,
  "\n"
);

console.log(
  "Single dataset NOT inclusive with extras (TAIL SERIE): ",
  extractScoresWithExtras(
    START_DATE_TAIL_OF_SERIE,
    END_DATE_TAIL_OF_SERIE,
    rawData
  ).scores.length,
  "\n"
);
console.log(
  "Double dataset NOT inclusive with extras (TAIL SERIE): ",
  extractScoresWithExtras(
    START_DATE_TAIL_OF_SERIE,
    END_DATE_TAIL_OF_SERIE,
    doubleRawData
  ).scores.length,
  "\n"
);
console.log(
  "Quadruple dataset NOT inclusive with extras (TAIL SERIE): ",
  extractScoresWithExtras(
    START_DATE_TAIL_OF_SERIE,
    END_DATE_TAIL_OF_SERIE,
    quadrupleRawData
  ).scores.length,
  "\n"
);
console.log(
  "Octuple dataset NOT inclusive with extras (TAIL SERIE): ",
  extractScoresWithExtras(
    START_DATE_TAIL_OF_SERIE,
    END_DATE_TAIL_OF_SERIE,
    octupleRawData
  ).scores.length,
  "\n"
);

console.log(
  "Single dataset inclusive with extras (HEAD SERIE): ",
  extractScoresInclusiveWithExtras(
    START_DATE_HEAD_OF_SERIE,
    END_DATE_HEAD_OF_SERIE,
    rawData
  ).scores.length,
  "\n"
);
console.log(
  "Double dataset inclusive with extras (HEAD SERIE): ",
  extractScoresInclusiveWithExtras(
    START_DATE_HEAD_OF_SERIE,
    END_DATE_HEAD_OF_SERIE,
    doubleRawData
  ).scores.length,
  "\n"
);
console.log(
  "Quadruple dataset inclusive with extras (HEAD SERIE): ",
  extractScoresInclusiveWithExtras(
    START_DATE_HEAD_OF_SERIE,
    END_DATE_HEAD_OF_SERIE,
    quadrupleRawData
  ).scores.length,
  "\n"
);
console.log(
  "Octuple dataset inclusive with extras (HEAD SERIE): ",
  extractScoresInclusiveWithExtras(
    START_DATE_HEAD_OF_SERIE,
    END_DATE_HEAD_OF_SERIE,
    octupleRawData
  ).scores.length,
  "\n"
);

console.log(
  "Single dataset inclusive with extras (TAIL SERIE): ",
  extractScoresInclusiveWithExtras(
    START_DATE_TAIL_OF_SERIE,
    END_DATE_TAIL_OF_SERIE,
    rawData
  ).scores.length,
  "\n"
);
console.log(
  "Double dataset inclusive with extras (TAIL SERIE): ",
  extractScoresInclusiveWithExtras(
    START_DATE_TAIL_OF_SERIE,
    END_DATE_TAIL_OF_SERIE,
    doubleRawData
  ).scores.length,
  "\n"
);
console.log(
  "Quadruple dataset inclusive with extras (TAIL SERIE): ",
  extractScoresInclusiveWithExtras(
    START_DATE_TAIL_OF_SERIE,
    END_DATE_TAIL_OF_SERIE,
    quadrupleRawData
  ).scores.length,
  "\n"
);
console.log(
  "Octuple dataset inclusive with extras (TAIL SERIE): ",
  extractScoresInclusiveWithExtras(
    START_DATE_TAIL_OF_SERIE,
    END_DATE_TAIL_OF_SERIE,
    octupleRawData
  ).scores.length,
  "\n"
);

observer.disconnect();
